package com.sakovkid.funnytrainer.data.database.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sakovkid.funnytrainer.data.repository.model.WarmUp

@Entity
data class WarmUpEntity(
    @PrimaryKey
    @NonNull
    val id: Long,                // -> ID разогрева
    val name: String,           // -> Название разогрева
    val count: Int,              // -> В секундах
    val description: String,     // -> Описание разогрева
    val url_gif: String,         // -> Ссылка на gif-анимацию разогрева
    val url_video: String        // -> Ссылка на видео разогрева
) {
    fun convertToWarmUp(): WarmUp {
        return WarmUp(
            this.id,
            this.name,
            this.count,
            this.description,
            this.url_gif,
            this.url_video
        )
    }
}