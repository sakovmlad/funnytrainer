package com.sakovkid.funnytrainer.data.database.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sakovkid.funnytrainer.data.repository.model.Program

@Entity
data class ProgramEntity(
    @PrimaryKey
    @NonNull
    val id: Long,                //
    val name: String,           //
    val url_image: String,      // -> Ссылка на изображение
    val days: Int,              // -> Количество всех id_workout
    val id_workoutDay: String   // -> workout_id = "[1,2,3,4,...]"    хранить так
) : Converter {
    fun convertToProgram(): Program {
        return Program(
            this.id,
            this.name,
            this.url_image,
            this.days,
            this.convertStringToListInt(id_workoutDay)
        )
    }



}