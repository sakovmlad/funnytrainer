package com.sakovkid.funnytrainer.data.database.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sakovkid.funnytrainer.data.repository.model.Hitch

@Entity
data class HitchEntity(
    @PrimaryKey
    @NonNull
    val id: Long,                // -> ID заминки
    val name: String,           // -> Название заминки
    val time: Int,              // -> В секундах
    val description: String,    // -> Описание заминки
    val url_gif: String,        // -> Ссылка на gif-анимацию заминки
    val url_video: String       // -> Ссылка на видео заминки
) {
    fun convertToHitch(): Hitch {
        return Hitch(
            this.id,
            this.name,
            this.time,
            this.description,
            this.url_gif,
            this.url_video
        )
    }
}
