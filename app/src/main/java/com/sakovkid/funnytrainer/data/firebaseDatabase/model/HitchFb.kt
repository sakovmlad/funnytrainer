package com.sakovkid.funnytrainer.data.firebaseDatabase.model

import com.sakovkid.funnytrainer.data.database.entity.HitchEntity
import com.sakovkid.funnytrainer.ui.infoWorkout.adapter.WorkoutAdapterModel

data class HitchFb(
    val id: Long,                // -> ID заминки
    val name: String,           // -> Название заминки
    val time: Int,              // -> В секундах
    val description: String,    // -> Описание заминки
    val url_gif: String,        // -> Ссылка на gif-анимацию заминки
    val url_video: String       // -> Ссылка на видео заминки
) {
    constructor():this(0,"",0,"","","")

    fun convertToHitchEntity(): HitchEntity {
        return HitchEntity(
            id,
            name,
            time,
            description,
            url_gif,
            url_video
        )
    }

    fun convertToWorkoutAdapterModel(): WorkoutAdapterModel {
        return WorkoutAdapterModel(
            id,
            name,
            time,
            0,
            description,
            url_gif,
            url_video
        )
    }
}