package com.sakovkid.funnytrainer.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sakovkid.funnytrainer.data.database.dao.DbDao
import com.sakovkid.funnytrainer.data.database.entity.*

@Database(
    entities = [
        ProgramEntity::class,
        WorkoutDayEntity::class,
        WorkoutEntity::class,
        WarmUpEntity::class,
        HitchEntity::class,
        UserEntity::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun dbDao(): DbDao
}