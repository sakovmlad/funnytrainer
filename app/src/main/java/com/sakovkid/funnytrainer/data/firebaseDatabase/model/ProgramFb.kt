package com.sakovkid.funnytrainer.data.firebaseDatabase.model

import com.sakovkid.funnytrainer.data.database.entity.ProgramEntity
import com.sakovkid.funnytrainer.data.repository.model.Program
import java.io.Serializable

data class ProgramFb(
    val id: Long,                   //
    val name: String,               //
    val url_image: String,          // -> Ссылка на изображение
    val days: Int,                  // -> Количество всех id_workout
    val id_workoutDay: List<Long>   //
) : Serializable {

    constructor() : this(0,"","",0, listOf())

    fun convertToProgram(): Program {
        return Program(
            id,
            name,
            url_image,
            days,
            id_workoutDay
        )
    }
}
