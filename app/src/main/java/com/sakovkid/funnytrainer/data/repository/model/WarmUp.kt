package com.sakovkid.funnytrainer.data.repository.model

import com.sakovkid.funnytrainer.data.database.entity.WarmUpEntity
import com.sakovkid.funnytrainer.data.firebaseDatabase.model.WarmUpFb
import com.sakovkid.funnytrainer.ui.infoWorkout.adapter.WorkoutAdapterModel

data class WarmUp(
    val id: Long,                // -> ID разогрева
    val name: String,           // -> Название разогрева
    val time: Int,              // -> В секундах
    val description: String,     // -> Описание разогрева
    val url_gif: String,         // -> Ссылка на gif-анимацию разогрева
    val url_video: String        // -> Ссылка на видео разогрева
) {
    fun convertToWarmUpEntity(): WarmUpEntity {
        return WarmUpEntity(
            id,
            name,
            time,
            description,
            url_gif,
            url_video
        )
    }
    fun convertToWarmUpFb(): WarmUpFb {
        return WarmUpFb(
            id,
            name,
            time,
            description,
            url_gif,
            url_video
        )
    }

    fun convertToWorkoutAdapterModel(): WorkoutAdapterModel {
        return WorkoutAdapterModel(
            id,
            name,
            time,
            0,
            description,
            url_gif,
            url_video
        )
    }
}
