package com.sakovkid.funnytrainer.data.database.dao

import androidx.room.*
import com.sakovkid.funnytrainer.data.database.entity.*
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface DbDao {
    /////////////////////
    /* Таблица Program */
    /////////////////////

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProgram(program: ProgramEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProgram(programs: List<ProgramEntity>)

    @Update
    fun updateProgram(program: ProgramEntity)

    @Delete
    fun deleteProgram(program: ProgramEntity)

    @Query("SELECT * FROM ProgramEntity")
    fun getAllProgram(): Single<List<ProgramEntity>>

    ////////////////////////
    /* Таблица WorkoutDay */
    ////////////////////////

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWorkoutDay(workoutDay: WorkoutDayEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWorkoutDay(workoutDay: List<WorkoutDayEntity>)

    @Update
    fun updateWorkoutDay(workoutDay: WorkoutDayEntity)

    @Delete
    fun deleteWorkoutDay(workoutDay: WorkoutDayEntity)

    @Query("SELECT * FROM WorkoutDayEntity")
    fun getAllWorkoutDay(): Maybe<List<WorkoutDayEntity>>

    @Query("SELECT * FROM WorkoutDayEntity WHERE id=:id")
    fun getWorkoutDayById(id:Int): Maybe<List<WorkoutDayEntity>>

    @Query("SELECT * FROM WorkoutDayEntity WHERE id IN (:idList)")
    fun getWorkoutDayById(idList:List<Long>): Maybe<List<WorkoutDayEntity>>

    ////////////////////
    /* Таблица Workout*/
    ////////////////////

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWorkout(program: WorkoutEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWorkout(programs: List<WorkoutEntity>)

    @Update
    fun updateWorkout(program: WorkoutEntity)

    @Delete
    fun deleteWorkout(program: WorkoutEntity)

    @Query("SELECT * FROM WorkoutEntity")
    fun getAllWorkout(): Maybe<List<WorkoutEntity>>

    @Query("SELECT * FROM WorkoutEntity WHERE id=:id")
    fun getWorkoutById(id:Int): Maybe<List<WorkoutEntity>>

    @Query("SELECT * FROM WorkoutEntity WHERE id IN (:idList)")
    fun getWorkoutById(idList:List<Long>): Maybe<List<WorkoutEntity>>

    ///////////////////
    /* Таблица WarmUp*/
    //////////////////

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWarmUp(warmUp: WarmUpEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWarmUp(warmUp: List<WarmUpEntity>)

    @Update
    fun updateWarmUp(warmUp: WarmUpEntity)

    @Delete
    fun deleteWarmUp(warmUp: WarmUpEntity)

    @Query("SELECT * FROM WarmUpEntity")
    fun getAllWarmUp(): Maybe<List<WarmUpEntity>>

    @Query("SELECT * FROM WarmUpEntity WHERE id=:id")
    fun getWarmUpById(id:Int): Maybe<List<WarmUpEntity>>

    @Query("SELECT * FROM WarmUpEntity WHERE id IN (:idList)")
    fun getWarmUpById(idList:List<Long>): Maybe<List<WarmUpEntity>>

    //////////////////
    /* Таблица Hitch*/
    //////////////////

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHitch(hitch: HitchEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHitch(hitch: List<HitchEntity>)

    @Update
    fun updateHitch(hitch: HitchEntity)

    @Delete
    fun deleteHitch(hitch: HitchEntity)

    @Query("SELECT * FROM HitchEntity")
    fun getAllHitch(): Maybe<List<HitchEntity>>

    @Query("SELECT * FROM HitchEntity WHERE id=:id")
    fun getHitchById(id:Int): Maybe<List<HitchEntity>>

    @Query("SELECT * FROM HitchEntity WHERE id IN (:idList)")
    fun getHitchById(idList:List<Long>): Maybe<List<HitchEntity>>

    //////////////////
    /* Таблица User*/
    //////////////////

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: UserEntity):Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: List<UserEntity>)

    @Update
    fun updateUser(user: UserEntity)

    @Delete
    fun deleteUser(user: UserEntity)

    @Query("SELECT * FROM UserEntity")
    fun getAllUser(): Maybe<List<UserEntity>>

    @Query("SELECT * FROM UserEntity WHERE id=:id")
    fun getUserById(id:Int): Maybe<UserEntity>

    @Query("SELECT * FROM UserEntity WHERE id=:id")
    fun getUserById(id:String): Maybe<UserEntity>

    @Query("SELECT * FROM UserEntity WHERE id IN (:idList)")
    fun getUserById(idList:List<Long>): Maybe<List<UserEntity>>



}