package com.sakovkid.funnytrainer.data.repository.model

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import com.sakovkid.funnytrainer.data.database.entity.UserEntity

data class User(
    val id: String,
    val name: String,
    val surname: String,
    val sex: String,
    val count_workout_completed: Long,
    val count_k_calories: Long
) {
    constructor() : this(FirebaseAuth.getInstance().currentUser?.uid.toString(),"","","",0,0)

    fun convertToUserEntity(): UserEntity {
        return UserEntity(
            id,
            name,
            surname,
            sex,
            count_workout_completed,
            count_k_calories
        )
    }
}