package com.sakovkid.funnytrainer.data.database.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sakovkid.funnytrainer.data.repository.model.User

@Entity
data class UserEntity(
    @PrimaryKey
    @NonNull
    val id: String,
    val name: String,
    val surname: String,
    val sex: String,
    val count_workout_completed: Long,
    val count_k_calories: Long
) {
    fun convertToUser(): User {
        return User(
            id,
            name,
            surname,
            sex,
            count_workout_completed,
            count_k_calories
        )
    }
}
