package com.sakovkid.funnytrainer.data.firebaseDatabase.model

import com.sakovkid.funnytrainer.data.database.entity.WorkoutEntity
import com.sakovkid.funnytrainer.data.database.entity.WorkoutType
import com.sakovkid.funnytrainer.ui.infoWorkout.adapter.WorkoutAdapterModel

data class WorkoutFb(
    val id: Long,
    val name: String,
    val type: String,       // type = TIME || REPS
    val count_reps: Int,    // Кол-во повторений
    val count_time: Int,    // Кол-во повторений
    val description: String,// Описание упражнения
    val url_gif: String,    // Анимация выполнения упражнения
    val url_video: String   // видео выполнения упражнения
) {
    constructor() : this(0,"","",0,0,"","","")

    fun convertToWorkoutEntity(): WorkoutEntity {
        return WorkoutEntity(
            id,
            name,
            type,
            count_reps,
            count_time,
            description,
            url_gif,
            url_video
        )
    }

    fun convertToWorkoutAdapterModel():WorkoutAdapterModel {
        return WorkoutAdapterModel(
            id,
            name,
            count_time,
            count_reps,
            description,
            url_gif,
            url_video
        )
    }
}
