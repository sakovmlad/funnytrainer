package com.sakovkid.funnytrainer.data.repository

import com.sakovkid.funnytrainer.data.database.dao.DbDao
import com.sakovkid.funnytrainer.data.firebaseDatabase.client.IFirebaseClient
import com.sakovkid.funnytrainer.data.firebaseDatabase.model.WorkoutFb
import com.sakovkid.funnytrainer.data.repository.model.*
import com.sakovkid.funnytrainer.ui.infoWorkout.adapter.WorkoutAdapterModel
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class Repository(private val dbClient: DbDao, private val firebaseClient: IFirebaseClient) {

    //////////////////////////
    /* Локальная База Данных*/
    //////////////////////////
    ////////////////////
    /* Таблица Program*/
    ////////////////////

    fun getProgram(): Single<List<Program>> {
        return Single.concat(
            getProgramFromDb(),
            getProgramFromFirebaseDb()
        ).filter { it.isNotEmpty() }
            .firstOrError()
            .onErrorReturn { error ->
                if (error is NoSuchElementException) {
                    return@onErrorReturn emptyList<Program>()
                }
                throw error
            }
    }

    fun getProgramFromDb(): Single<List<Program>> {
        return dbClient.getAllProgram()
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToProgram() }
            .toList()
    }


    fun setUserToDb(user: User): Completable {
        return dbClient.insertUser(user.convertToUserEntity())
    }

    fun getUserToDb(id: String): Single<User> {
        return dbClient.getUserById(id).map { it.convertToUser() }.toSingle()
    }

    fun setProgramToFirebaseDb(program: Program): Completable {
        return firebaseClient.setProgramToFirebaseDb(program)
    }

    fun getProgramFromFirebaseDb(): Single<List<Program>> {
        return firebaseClient.getProgramFromFirebaseDb()
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToProgram() }
            .toList()
    }

    fun getWorkoutAdapterModule(
        idWarmUp: List<Long>,
        idWorkout: List<Long>,
        idHitch: List<Long>
    ): Flowable<MutableList<WorkoutAdapterModel>> {

        return Single.concat(
            getWarmUpById(idWarmUp).subscribeOn(Schedulers.io()).toObservable()
                .flatMapIterable { it }
                .map { it.convertToWorkoutAdapterModel() }.toList(),
            getWorkoutById(idWorkout).subscribeOn(Schedulers.io()).toObservable()
                .flatMapIterable { it }
                .map { it.convertToWorkoutAdapterModel() }.toList(),
            getHitchById(idHitch).subscribeOn(Schedulers.io()).toObservable().flatMapIterable { it }
                .map { it.convertToWorkoutAdapterModel() }.toList()
        )
    }


    fun getWorkoutById(id: Int): Single<List<Workout>> {
        return dbClient.getWorkoutById(id)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToWorkout() }
            .toList()
    }

    fun getWorkoutById(id: List<Long>): Single<List<Workout>> {
        return dbClient.getWorkoutById(id)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToWorkout() }
            .toList()
    }

    fun getWarmUpById(id: Int): Single<List<WarmUp>> {
        return dbClient.getWarmUpById(id)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToWarmUp() }
            .toList()
    }

    fun getWarmUpById(id: List<Long>): Single<List<WarmUp>> {
        return dbClient.getWarmUpById(id)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToWarmUp() }
            .toList()
    }


    fun getHitchById(id: Int): Single<List<Hitch>> {
        return dbClient.getHitchById(id)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToHitch() }
            .toList()
    }

    fun getHitchById(id: List<Long>): Single<List<Hitch>> {
        return dbClient.getHitchById(id)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToHitch() }
            .toList()
    }

    fun getWorkoutDayById(id: Int): Single<List<WorkoutDay>> {
        return dbClient.getWorkoutDayById(id)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToWorkoutDay() }
            .toList()
    }

    fun getWorkoutDayById(id: List<Long>): Single<List<WorkoutDay>> {
        return dbClient.getWorkoutDayById(id)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToWorkoutDay() }
            .toList()

    }

    fun setWorkoutToDb(workout: List<Workout>): Single<Boolean> {
        return Single.just(workout)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToWorkoutEntity() }
            .toList()
            .doOnSuccess { dbClient.insertWorkout(it) }
            .map { true }
    }

    fun setWorkoutToFirebaseDb(workout: List<Workout>): Completable {
        val a: MutableList<WorkoutFb> = mutableListOf()
        workout.forEach {
            a.add(it.convertToWorkoutFb())
        }
        return firebaseClient.setWorkoutToFirebaseDb(a)

    }

    fun setWorkoutToDb(workout: Workout): Single<Boolean> {
        return Single.just(workout)
            .map { it.convertToWorkoutEntity() }
            .doOnSuccess { dbClient.insertWorkout(it) }
            .map { true }
    }

    fun setWarmUpToFirebaseDb(warmUp: WarmUp): Completable {
        return firebaseClient.setWarmUp(warmUp.convertToWarmUpFb())
    }

    fun setProgramToDb(program: Program): Single<Boolean> {
        return Single.just(program)
            .map { it.convertToProgramEntity() }
            .doOnSuccess { dbClient.insertProgram(it) }
            .map { true }
    }

    fun setProgramToDb(program: List<Program>): Single<Boolean> {
        return Single.just(program)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToProgramEntity() }
            .toList()
            .doOnSuccess { dbClient.insertProgram(it) }
            .map { true }
    }

    fun setWorkoutDayToDb(workoutDay: WorkoutDay): Single<Boolean> {
        return Single.just(workoutDay)
            .map { it.convertToWorkoutDayEntity() }
            .doOnSuccess { dbClient.insertWorkoutDay(it) }
            .map { true }
    }

    fun setWorkoutDayToDb(workoutDay: List<WorkoutDay>): Single<Boolean> {
        return Single.just(workoutDay)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToWorkoutDayEntity() }
            .toList()
            .doOnSuccess { dbClient.insertWorkoutDay(it) }
            .map { true }
    }

    fun setWarmUpToDb(warmUp: WarmUp): Single<Boolean> {
        return Single.just(warmUp)
            .map { it.convertToWarmUpEntity() }
            .doOnSuccess { dbClient.insertWarmUp(it) }
            .map { true }
    }

    fun setWarmUpToDb(warmUp: List<WarmUp>): Single<Boolean> {
        return Single.just(warmUp)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToWarmUpEntity() }
            .toList()
            .doOnSuccess { dbClient.insertWarmUp(it) }
            .map { true }
    }

    fun setHitchToDb(hitch: Hitch): Single<Boolean> {
        return Single.just(hitch)
            .map { it.convertToHitchEntity() }
            .doOnSuccess { dbClient.insertHitch(it) }
            .map { true }
    }

    fun setHitchToDb(hitch: List<Hitch>): Single<Boolean> {
        return Single.just(hitch)
            .toObservable()
            .flatMapIterable { it }
            .map { it.convertToHitchEntity() }
            .toList()
            .doOnSuccess { dbClient.insertHitch(it) }
            .map { true }
    }


}
