package com.sakovkid.funnytrainer.data.firebaseDatabase

import com.google.firebase.database.*
import com.sakovkid.funnytrainer.data.firebaseDatabase.model.ProgramFb
import com.sakovkid.funnytrainer.data.repository.model.Program
import com.sakovkid.funnytrainer.data.repository.model.Workout
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

class RxFirebaseDatabase {

    fun setValue(query: DatabaseReference, value:Any): Completable {
        return Completable.create{ emitter ->
            query.setValue(value)
                .addOnSuccessListener { emitter.onComplete() }
                .addOnFailureListener { emitter.onError(it) }
        }
    }

    fun getValue(query: DatabaseReference): Single<List<ProgramFb>> {
        return Single.create{ emitter ->
            query.addValueEventListener(object :ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    val list : MutableList<ProgramFb> = mutableListOf()
                    snapshot.children.forEach{
                        if(it.getValue(ProgramFb::class.java) != null) {
                            list.add(it.getValue(ProgramFb::class.java)!!)
                        }
                    }
                    emitter.onSuccess(list)
                }
                override fun onCancelled(error: DatabaseError) {
                    emitter.onError(error.toException())
                }
            })
        }
    }

    fun addChildEventListener(query: DatabaseReference): Single<DataSnapshot> {
        return Single.create<DataSnapshot> { emitter ->
            query.addChildEventListener(object : ChildEventListener {
                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {

                }

                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                    TODO("Not yet implemented")
                }

                override fun onChildRemoved(snapshot: DataSnapshot) {
                    TODO("Not yet implemented")
                }

                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                    TODO("Not yet implemented")
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
            })
        }
    }

    fun addValueEventListener(query: DatabaseReference): Single<DataSnapshot> {
        return Single.create<DataSnapshot> { emitter ->
            query.addValueEventListener(object :ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    TODO("Not yet implemented")
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
            })
        }
    }

    fun addListenerForSingleValueEvent(query: DatabaseReference): Single<Workout> {
        return Single.create<Workout> { emitter ->
            query.addListenerForSingleValueEvent(object :ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                }

                override fun onCancelled(error: DatabaseError) {
                    emitter.onError(error.toException())
                }
            })
        }
    }
}