package com.sakovkid.funnytrainer.data.firebaseDatabase.client

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.sakovkid.funnytrainer.data.firebaseDatabase.RxFirebaseDatabase
import com.sakovkid.funnytrainer.data.firebaseDatabase.model.ProgramFb
import com.sakovkid.funnytrainer.data.firebaseDatabase.model.WarmUpFb
import com.sakovkid.funnytrainer.data.firebaseDatabase.model.WorkoutFb
import com.sakovkid.funnytrainer.data.repository.model.Program
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FirebaseClient(private val rxFirebaseDatabase: RxFirebaseDatabase) : IFirebaseClient {

    // Root
    private val root: DatabaseReference = FirebaseDatabase.getInstance().reference.root

    /////////////////////
    /* Таблица Program */
    /////////////////////
    private val PROGRAM = "program"
    private var ID_PROGRAMS = root.child(PROGRAM).push().key.toString()
    private val ID_PROGRAM = ID_PROGRAMS
    private val NAME = "name"
    private val URL_IMAGE = "url_image"
    private val DAYS = "days"
    private val LIST_ID_WORKOUT_DAY = "id_workoutDay"

    ////////////////////////
    /* Таблица WorkoutDay */
    ////////////////////////
    private val WORKOUT_DAY = "workout_day"
    private var ID_WORKOUT_DAYS = root.child(WORKOUT_DAY).push().key.toString()
    private val ID_WORKOUT_DAY = ID_WORKOUT_DAYS

    //              private val NAME =              "name"
    private val COMPLETED = "completed"
    private val TIMESTAMP = "timestamp"
    private val CIRCLE = "circle"
    private val COUNT_WORKOUT = "count_workout"
    private val TIME_WORKOUT = "time_workout"
    private val LIST_ID_WARM_UP = "id_warmUp"
    private val LIST_ID_WORKOUT = "id_workout"
    private val LIST_ID_HITCH = "id_hitch"

    /////////////////////
    /* Таблица Workout */
    /////////////////////
    private val WORKOUT = "workout"
    private var ID_WORKOUTS = root.child(WORKOUT).push().key.toString()
    private val ID_WORKOUT = ID_WORKOUTS

    //              private val NAME =          "name"
    private val TYPE = "type"
    private val COUNT_REPS = "count_reps"
    private val COUNT_TIME = "count_time"
    private val DESCRIPTION = "description"
    private val URL_GIF = "url_gif"
    private val URL_VIDEO = "url_video"

    ////////////////////
    /* Таблица WarmUp */
    ////////////////////
    private val WARM_UP = "warm_up"
    private var ID_WARM_UPS = root.child(WARM_UP).push().key.toString()
    private val ID_WARM_UP = ID_WARM_UPS

    //              private val NAME =          "name"
    private val TIME = "time"
//              private val DESCRIPTION =   "description"
//              private val URL_GIF =       "url_gif"
//              private val URL_VIDEO =     "url_video"

    ///////////////////
    /* Таблица Hitch */
    ///////////////////
    private val HITCH = "hitch"
    private var ID_HITCHS = root.child(HITCH).push().key.toString()
    private val ID_HITCH = ID_HITCHS
//              private val NAME =          "name"
//              private val TIME =          "time"
//              private val DESCRIPTION =   "description"
//              private val URL_GIF =       "url_gif"
//              private val URL_VIDEO =     "url_video"


    override fun setProgramToFirebaseDb(value: Program): Completable {
        val query = root
            .child(PROGRAM)
            .child(value.id.toString())
        return rxFirebaseDatabase.setValue(query, value.convertToProgramFb())
    }

    override fun getProgramFromFirebaseDb(): Single<List<ProgramFb>> {
        val query = root
            .child(PROGRAM)
        return rxFirebaseDatabase.getValue(query)
    }

    override fun setWorkoutToFirebaseDb(workout: List<WorkoutFb>): Completable {

        var complete = true
        workout.forEach {
            ID_WORKOUTS = updateIdWorkoutKey()
            val query = root
                .child(WORKOUT)
                .child(ID_WORKOUTS)
            rxFirebaseDatabase.setValue(query, it).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({},
                    { complete = false })
        }
        return if (complete) Completable.complete()
        else Completable.never()
    }

    override fun setWarmUp(warmUp: WarmUpFb): Completable {
        val query = root
            .child(WARM_UP)
            .child(ID_WARM_UPS)
        return rxFirebaseDatabase.setValue(query, warmUp)
    }


    private fun updateIdWorkoutKey(): String =
        root.child(WORKOUT).push().key.toString()


}