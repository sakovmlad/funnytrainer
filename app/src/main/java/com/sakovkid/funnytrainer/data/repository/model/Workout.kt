package com.sakovkid.funnytrainer.data.repository.model

import com.sakovkid.funnytrainer.data.database.entity.WorkoutEntity
import com.sakovkid.funnytrainer.data.database.entity.WorkoutType
import com.sakovkid.funnytrainer.data.firebaseDatabase.model.WorkoutFb
import com.sakovkid.funnytrainer.ui.infoWorkout.adapter.WorkoutAdapterModel

data class Workout(
    val id: Long,
    val name: String,
    val type: WorkoutType,       // type = TIME || REPS
    val count_reps: Int,    // Кол-во повторений
    val count_time: Int,    // Кол-во повторений
    val description: String,// Описание упражнения
    val url_gif: String,    // Анимация выполнения упражнения
    val url_video: String   // видео выполнения упражнения
) {
    fun convertToWorkoutEntity(): WorkoutEntity {
        return WorkoutEntity(
            id,
            name,
            type.name,
            count_reps,
            count_time,
            description,
            url_gif,
            url_video
        )
    }
    fun convertToWorkoutFb(): WorkoutFb {
        return WorkoutFb(
            id,
            name,
            type.name,
            count_reps,
            count_time,
            description,
            url_gif,
            url_video
        )
    }

    fun convertToWorkoutAdapterModel():WorkoutAdapterModel {
        return WorkoutAdapterModel(
            id,
            name,
            count_time,
            count_reps,
            description,
            url_gif,
            url_video
        )
    }
}
