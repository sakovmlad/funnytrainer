package com.sakovkid.funnytrainer.data.database.entity

interface Converter {
    fun convertStringToListInt(text: String): List<Long> {
        val a: MutableList<Long> = mutableListOf()
        var b = ""
        text.forEach {
            if (it != '[' && it != ']' && it != ' ') {
                if (it != ',') {
                    b += it.toString()
                } else {
                    a.add(b.toLong())
                    b = ""
                }
            }
            if (it == ']') {
                a.add(b.toLong())
            }
        }
        return a
    }
}