package com.sakovkid.funnytrainer.data.firebaseDatabase.client

import com.sakovkid.funnytrainer.data.firebaseDatabase.model.ProgramFb
import com.sakovkid.funnytrainer.data.firebaseDatabase.model.WarmUpFb
import com.sakovkid.funnytrainer.data.firebaseDatabase.model.WorkoutFb
import com.sakovkid.funnytrainer.data.repository.model.Program
import com.sakovkid.funnytrainer.data.repository.model.WarmUp
import io.reactivex.Completable
import io.reactivex.Single

interface IFirebaseClient {
    fun setProgramToFirebaseDb(program: Program): Completable

    fun getProgramFromFirebaseDb():Single<List<ProgramFb>>
    fun setWorkoutToFirebaseDb(workout: List<WorkoutFb>):Completable

    fun setWarmUp(warmUp: WarmUpFb):Completable
}