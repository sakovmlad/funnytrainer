package com.sakovkid.funnytrainer.data.repository.model

import com.sakovkid.funnytrainer.data.database.entity.ProgramEntity
import com.sakovkid.funnytrainer.data.firebaseDatabase.model.ProgramFb
import java.io.Serializable

data class Program(
    val id: Long,                   //
    val name: String,               //
    val url_image: String,          // -> Ссылка на изображение
    val days: Int,                  // -> Количество всех id_workout
    val id_workoutDay: List<Long>   //
) : Serializable {

    fun convertToProgramEntity(): ProgramEntity {
        return ProgramEntity(
            id,
            name,
            url_image,
            days,
            id_workoutDay.toString()
        )
    }

    fun convertToProgramFb(): ProgramFb {
        return ProgramFb(
            id,
            name,
            url_image,
            days,
            id_workoutDay
        )
    }
}
