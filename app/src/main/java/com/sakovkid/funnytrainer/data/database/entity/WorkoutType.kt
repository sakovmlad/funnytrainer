package com.sakovkid.funnytrainer.data.database.entity

enum class WorkoutType(val type: String) {
    TIME("TIME"),
    REPS("REPS")
}