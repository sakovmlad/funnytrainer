package com.sakovkid.funnytrainer.data.database.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sakovkid.funnytrainer.data.repository.model.WorkoutDay

@Entity
data class WorkoutDayEntity(
    @PrimaryKey
    @NonNull
    val id: Long,
    val name: String,
    val completed:Boolean,
    val timeStamp: Long,       // -> Дата выполнения упражнений
    val circle: Int,        // -> Кол-круг, повторений всех id_workout
    val count_workout: Int, // -> count_workout = кол-во всех упражнений + разогрев + заминка
    val time_workout: Int,  // -> time_workout = count_workout * 60 (сек)
    val id_warmUp: String,  // -> id_warmUp = "{1,2}"   хранить так
    val id_workout: String, // -> id_workout = "{1,2,3,4,...}"  хранить так
    val id_hitch: String    // -> id_hitch = "{1,2}"    хранить так
) : Converter {
    fun convertToWorkoutDay(): WorkoutDay {
        return WorkoutDay(
            id,
            name,
            completed,
            timeStamp,
            circle,
            count_workout,
            time_workout,
            convertStringToListInt(id_warmUp),
            convertStringToListInt(id_workout),
            convertStringToListInt(id_hitch)
        )
    }
}