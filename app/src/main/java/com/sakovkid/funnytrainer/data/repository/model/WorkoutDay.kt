package com.sakovkid.funnytrainer.data.repository.model

import com.sakovkid.funnytrainer.data.database.entity.WorkoutDayEntity
import java.io.Serializable
import java.util.*

data class WorkoutDay(
    val id: Long,
    val name: String,
    val completed: Boolean,
    val timeStamp: Long,             // -> Дата выполнения упражнений
    val circle: Int,            // -> Кол-круг, повторений всех id_workout
    val count_workout: Int,     // -> count_workout = кол-во всех упражнений + разогрев + заминка
    val time_workout: Int,      // -> time_workout = count_workout * 60 (сек)
    val id_warmUp: List<Long>,  // -> id_warmUp = "{1,2}"   хранить так
    val id_workout: List<Long>, // -> id_workout = "{1,2,3,4,...}"  хранить так
    val id_hitch: List<Long>    // -> id_hitch = "{1,2}"    хранить так
) : Serializable {
    constructor() : this(
        1,
        "День",
        false,
        1230421204,
        1,
        1,
        23,
        listOf(1),
        listOf(1),
        listOf(1)
    )


    fun convertToWorkoutDayEntity(): WorkoutDayEntity {
        return WorkoutDayEntity(
            id,
            name,
            completed,
            timeStamp,
            circle,
            count_workout,
            time_workout,
            id_warmUp.toString(),
            id_workout.toString(),
            id_hitch.toString()
        )
    }

    fun getYear(): Int {
        return getCalendar().get(Calendar.YEAR)
    }

    fun getMonth(): Int {
        return getCalendar().get(Calendar.MONTH)
    }

    fun getDay(): Int {
        return getCalendar().get(Calendar.DAY_OF_MONTH)
    }

    fun getCalendar(): Calendar {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timeStamp
        return calendar
    }

    fun getEmpty(): WorkoutDay {
        return WorkoutDay(
            1,
            "День",
            false,
            1230421204,
            1,
            1,
            23,
            listOf(1),
            listOf(1),
            listOf(1)
        )
    }

}
