package com.sakovkid.funnytrainer.data.firebaseDatabase.model

import com.sakovkid.funnytrainer.data.database.entity.UserEntity

data class UserFb(
    val id: String,
    val name: String,
    val surname: String,
    val sex: String,
    val count_workout_completed: Long,
    val count_k_calories: Long
) {
    constructor() : this("","","","",0,0)

    fun convertToUserEntity(): UserEntity {
        return UserEntity(
            id,
            name,
            surname,
            sex,
            count_workout_completed,
            count_k_calories
        )
    }
}