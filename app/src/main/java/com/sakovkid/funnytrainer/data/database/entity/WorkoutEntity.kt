package com.sakovkid.funnytrainer.data.database.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sakovkid.funnytrainer.data.repository.model.Workout

@Entity
data class WorkoutEntity(
    @PrimaryKey
    @NonNull
    val id: Long,
    val name: String,
    val type: String,       // type = TIME || REPS
    val count_reps: Int,    // Кол-во повторений
    val count_time: Int,    // Кол-во повторений
    val description: String,// Описание упражнения
    val url_gif: String,    // Анимация выполнения упражнения
    val url_video: String   // видео выполнения упражнения
) {
    fun convertToWorkout(): Workout {
        return Workout(
            this.id,
            this.name,
            WorkoutType.valueOf(type),
            this.count_reps,
            this.count_time,
            this.description,
            this.url_gif,
            this.url_video
        )
    }
}