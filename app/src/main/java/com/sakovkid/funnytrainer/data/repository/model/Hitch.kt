package com.sakovkid.funnytrainer.data.repository.model

import com.sakovkid.funnytrainer.data.database.entity.HitchEntity
import com.sakovkid.funnytrainer.ui.infoWorkout.adapter.WorkoutAdapterModel

data class Hitch(
    val id: Long,                // -> ID заминки
    val name: String,           // -> Название заминки
    val time: Int,              // -> В секундах
    val description: String,    // -> Описание заминки
    val url_gif: String,        // -> Ссылка на gif-анимацию заминки
    val url_video: String       // -> Ссылка на видео заминки
) {
    fun convertToHitchEntity(): HitchEntity {
        return HitchEntity(
            id,
            name,
            time,
            description,
            url_gif,
            url_video
        )
    }

    fun convertToWorkoutAdapterModel(): WorkoutAdapterModel {
        return WorkoutAdapterModel(
            id,
            name,
            time,
            0,
            description,
            url_gif,
            url_video
        )
    }
}