package com.sakovkid.funnytrainer.di

import com.sakovkid.funnytrainer.data.database.dao.DbDao
import com.sakovkid.funnytrainer.data.firebaseDatabase.client.IFirebaseClient
import com.sakovkid.funnytrainer.data.repository.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideProgramRepository(dbClient: DbDao, firebaseClient: IFirebaseClient): Repository =
        Repository(dbClient,firebaseClient)
}