package com.sakovkid.funnytrainer.di

import android.content.Context
import com.sakovkid.funnytrainer.service.MediaPlayer
import com.sakovkid.funnytrainer.service.Timer
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context = context

    @Provides
    @Singleton
    fun provideTimer(): Timer = Timer()

    @Provides
    @Singleton
    fun provideMediaPlayer(context: Context): MediaPlayer = MediaPlayer(context)

}