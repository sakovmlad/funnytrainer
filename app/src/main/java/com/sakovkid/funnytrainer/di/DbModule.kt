package com.sakovkid.funnytrainer.di

import android.content.Context
import androidx.room.Room
import com.sakovkid.funnytrainer.data.database.AppDatabase
import com.sakovkid.funnytrainer.data.database.dao.DbDao
import com.sakovkid.funnytrainer.data.firebaseDatabase.RxFirebaseDatabase
import com.sakovkid.funnytrainer.data.firebaseDatabase.client.FirebaseClient
import com.sakovkid.funnytrainer.data.firebaseDatabase.client.IFirebaseClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase =
        Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "FUNNYTRAINER_DB")
            .build()

    @Provides
    @Singleton
    fun provideProgramDao(db: AppDatabase): DbDao = db.dbDao()

    @Provides
    @Singleton
    fun provideFirebaseDatabase(rxFirebaseDatabase: RxFirebaseDatabase): IFirebaseClient =
        FirebaseClient(rxFirebaseDatabase)

    @Provides
    @Singleton
    fun provideRxFirebaseDatabase(): RxFirebaseDatabase = RxFirebaseDatabase()
}