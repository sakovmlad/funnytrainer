package com.sakovkid.funnytrainer.di

import com.sakovkid.funnytrainer.ui.activity.splashScreen.SplashScreenActivity
import com.sakovkid.funnytrainer.ui.calendarWorkout.CalendarWorkoutViewModel
import com.sakovkid.funnytrainer.ui.calendarWorkout.PageViewModel
import com.sakovkid.funnytrainer.ui.infoWorkout.InfoWorkoutViewModel
import com.sakovkid.funnytrainer.ui.profile.ProfileViewModel
import com.sakovkid.funnytrainer.ui.program.ProgramViewModel
import com.sakovkid.funnytrainer.ui.settings.SettingsViewModel
import com.sakovkid.funnytrainer.ui.timerBegin.BeginWorkoutViewModel
import com.sakovkid.funnytrainer.ui.timerWorkout.DoWorkoutViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, DbModule::class, RepositoryModule::class])
interface AppComponent {

    fun inject(viewModel: ProgramViewModel)
    fun inject(viewModel: InfoWorkoutViewModel)
    fun inject(viewModel: CalendarWorkoutViewModel)
    fun inject(viewModel: PageViewModel)
    fun inject(viewModel: ProfileViewModel)
    fun inject(viewModel: SettingsViewModel)
    fun inject(viewModel: DoWorkoutViewModel)
    fun inject(viewModel: BeginWorkoutViewModel)


    fun inject(activity: SplashScreenActivity)

}