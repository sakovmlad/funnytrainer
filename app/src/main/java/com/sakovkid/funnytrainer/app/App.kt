package com.sakovkid.funnytrainer.app

import android.app.Application
import com.google.firebase.auth.FirebaseAuth
import com.sakovkid.funnytrainer.di.AppComponent
import com.sakovkid.funnytrainer.di.AppModule
import com.sakovkid.funnytrainer.di.DaggerAppComponent
import com.sakovkid.funnytrainer.di.DbModule

class App : Application() {

    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        // Анонимный вход в Firebase
        FirebaseAuth.getInstance().signInAnonymously()

        component = DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()
    }
}