package com.sakovkid.funnytrainer.ui.calendarWorkout.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.sakovkid.funnytrainer.R
import com.sakovkid.funnytrainer.data.repository.model.WorkoutDay
import com.sakovkid.funnytrainer.ui.calendarWorkout.PlaceholderFragment

private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2,
    R.string.tab_text_3,
    R.string.tab_text_4
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager, val workoutDay: List<WorkoutDay>)
    : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        var i = 0
        val value:MutableList<WorkoutDay> = mutableListOf()
        when(position) {
            0 -> value.addAll(workoutDay.subList(position * 6 + position, position * 6 + position + 7))
            1 -> value.addAll(workoutDay.subList(position * 6 + position, position * 6 + position + 7))
            2 -> value.addAll(workoutDay.subList(position * 6 + position, position * 6 + position + 7))
            3 -> value.addAll(workoutDay.subList(position * 6 + position, position * 6 + position + 7))
        }
        return PlaceholderFragment.newInstance(position + 1,value)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }


    override fun getCount(): Int {
        // Show 2 total pages.
        return 4
    }
}