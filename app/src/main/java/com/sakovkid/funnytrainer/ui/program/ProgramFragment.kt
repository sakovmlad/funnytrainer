package com.sakovkid.funnytrainer.ui.program

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.sakovkid.funnytrainer.R
import com.sakovkid.funnytrainer.data.repository.model.Program
import com.sakovkid.funnytrainer.ui.program.adapter.OnProgramClickListener
import com.sakovkid.funnytrainer.ui.program.adapter.ProgramAdapter

class ProgramFragment() : Fragment() {

    lateinit var programViewModel: ProgramViewModel

    private var adapter: ProgramAdapter = ProgramAdapter(object : OnProgramClickListener {
        override fun onClickProgram(program: Program) {
            val arg = Bundle()
            arg.putSerializable("program", program)

            findNavController().navigate(R.id.action_program_to_calendar_workout, arg)
        }
    })
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_program, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        programViewModel =
            ViewModelProvider(this).get(ProgramViewModel::class.java)

        val viewProgram = view.findViewById<RecyclerView>(R.id.recycler_program)
        viewProgram.adapter = adapter

        programViewModel.program.observe(viewLifecycleOwner, Observer {
            adapter.replaceDataSet(it)
        })
        programViewModel.text.observe(viewLifecycleOwner, Observer {

        })

    }
}