package com.sakovkid.funnytrainer.ui.calendarWorkout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.sakovkid.funnytrainer.R
import com.sakovkid.funnytrainer.data.repository.model.WorkoutDay

/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : Fragment() {

    private lateinit var pageViewModel: PageViewModel
    private var workoutDay: MutableList<WorkoutDay> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        val program = this.arguments?.getSerializable("program") as Program
        pageViewModel = ViewModelProvider(this).get(PageViewModel::class.java).apply {

            val a: MutableList<WorkoutDay> = mutableListOf()
            a.add(arguments?.getSerializable(DAY_1) as? WorkoutDay ?: WorkoutDay())
            a.add(arguments?.getSerializable(DAY_2) as? WorkoutDay ?: WorkoutDay())
            a.add(arguments?.getSerializable(DAY_3) as? WorkoutDay ?: WorkoutDay())
            a.add(arguments?.getSerializable(DAY_4) as? WorkoutDay ?: WorkoutDay())
            a.add(arguments?.getSerializable(DAY_5) as? WorkoutDay ?: WorkoutDay())
            a.add(arguments?.getSerializable(DAY_6) as? WorkoutDay ?: WorkoutDay())
            a.add(arguments?.getSerializable(DAY_7) as? WorkoutDay ?: WorkoutDay())

            setIndex(
                arguments?.getInt(ARG_SECTION_NUMBER) ?: 1, a
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)
        val tvDay1: TextView = root.findViewById(R.id.tv_Day_1)
        val tvDay2: TextView = root.findViewById(R.id.tv_Day_2)
        val tvDay3: TextView = root.findViewById(R.id.tv_Day_3)
        val tvDay4: TextView = root.findViewById(R.id.tv_Day_4)
        val tvDay5: TextView = root.findViewById(R.id.tv_Day_5)
        val tvDay6: TextView = root.findViewById(R.id.tv_Day_6)
        val tvDay7: TextView = root.findViewById(R.id.tv_Day_7)
        val constLayout1: ConstraintLayout = root.findViewById(R.id.const_layout_1)
        val constLayout2: ConstraintLayout = root.findViewById(R.id.const_layout_2)
        val constLayout3: ConstraintLayout = root.findViewById(R.id.const_layout_3)
        val constLayout4: ConstraintLayout = root.findViewById(R.id.const_layout_4)
        val constLayout5: ConstraintLayout = root.findViewById(R.id.const_layout_5)
        val constLayout6: ConstraintLayout = root.findViewById(R.id.const_layout_6)
        val constLayout7: ConstraintLayout = root.findViewById(R.id.const_layout_7)

        val arg = Bundle()
        constLayout1.setOnClickListener {
            arg.putSerializable("workout", workoutDay[0])
            findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)

        }
        constLayout2.setOnClickListener {
            arg.putSerializable("workout", workoutDay[1])
            findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
        }
        constLayout3.setOnClickListener {
            arg.putSerializable("workout", workoutDay[2])
            findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
        }
        constLayout4.setOnClickListener {
            arg.putSerializable("workout", workoutDay[3])
            findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
        }
        constLayout5.setOnClickListener {
            arg.putSerializable("workout", workoutDay[4])
            findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
        }
        constLayout6.setOnClickListener {
            arg.putSerializable("workout", workoutDay[5])
            findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
        }
        constLayout7.setOnClickListener {
            arg.putSerializable("workout", workoutDay[6])
            findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
        }
//        View.OnClickListener {
//            val arg = Bundle()
//            when(it){
//                constLayout1 -> {
//                    arg.putSerializable("workout", workoutDay[0])
//                    findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
//                }
//                constLayout2 -> {
//                    arg.putSerializable("workout", workoutDay[1])
//                    findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
//                }
//                constLayout3 -> {
//                    arg.putSerializable("workout", workoutDay[2])
//                    findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
//                }
//                constLayout4 -> {
//                    arg.putSerializable("workout", workoutDay[3])
//                    findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
//                }
//                constLayout5 -> {
//                    arg.putSerializable("workout", workoutDay[4])
//                    findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
//                }
//                constLayout6 -> {
//                    arg.putSerializable("workout", workoutDay[5])
//                    findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
//                }
//                constLayout7 -> {
//                    arg.putSerializable("workout", workoutDay[6])
//                    findNavController().navigate(R.id.action_calendarWorkoutFragment_to_infoWorkoutFragment, arg)
//                }
//            }
//        }

        pageViewModel.text.observe(this.viewLifecycleOwner, Observer<List<WorkoutDay>> {
            workoutDay.clear()
            workoutDay.addAll(it)

            tvDay1.text = it[0].name
            tvDay2.text = it[1].name
            tvDay3.text = it[2].name
            tvDay4.text = it[3].name
            tvDay5.text = it[4].name
            tvDay6.text = it[5].name
            tvDay7.text = it[6].name

            constLayout1.isClickable = !it[0].completed
            constLayout2.isClickable = !it[1].completed
            constLayout3.isClickable = !it[2].completed
            constLayout4.isClickable = !it[3].completed
            constLayout5.isClickable = !it[4].completed
            constLayout6.isClickable = !it[5].completed
            constLayout7.isClickable = !it[6].completed
        })
        return root
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"
        private const val DAY_1 = "day_1"
        private const val DAY_2 = "day_2"
        private const val DAY_3 = "day_3"
        private const val DAY_4 = "day_4"
        private const val DAY_5 = "day_5"
        private const val DAY_6 = "day_6"
        private const val DAY_7 = "day_7"
        private const val WEEK = "week"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int, week: List<WorkoutDay>): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(DAY_1, week[0])
                    putSerializable(DAY_2, week[1])
                    putSerializable(DAY_3, week[2])
                    putSerializable(DAY_4, week[3])
                    putSerializable(DAY_5, week[4])
                    putSerializable(DAY_6, week[5])
                    putSerializable(DAY_7, week[6])
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}