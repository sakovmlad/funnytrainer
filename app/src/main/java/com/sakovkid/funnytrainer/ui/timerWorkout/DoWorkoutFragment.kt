package com.sakovkid.funnytrainer.ui.timerWorkout

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import com.sakovkid.funnytrainer.R

class DoWorkoutFragment : Fragment() {

    private lateinit var textViewTime: TextView
    private lateinit var button: Button

    private lateinit var viewModel: DoWorkoutViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.timer_fragment, container, false)
        textViewTime = root.findViewById(R.id.text_time)
        button = root.findViewById(R.id.button_pause)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DoWorkoutViewModel::class.java)
        button.setOnClickListener {
//            viewModel.plus(5000)

        }

        viewModel.time.observe(viewLifecycleOwner, Observer {
//            textViewTime.text = it
        })
    }


    private fun convertToString(time: Long): String {

        var textTime = "00:00"

        textTime = if ((time / 60).toString().length == 1) "0" + (time / 60).toString()
        else (time / 60).toString()

        textTime = if ((time % 60).toString().length == 1) textTime + ":0" + (time % 60).toString()
        else textTime + ":" + (time % 60).toString()

        return textTime
    }
}