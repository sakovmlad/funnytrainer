package com.sakovkid.funnytrainer.ui.calendarWorkout

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sakovkid.funnytrainer.app.App
import com.sakovkid.funnytrainer.data.repository.Repository
import com.sakovkid.funnytrainer.data.repository.model.WorkoutDay
import javax.inject.Inject

class PageViewModel() : ViewModel() {
    @Inject
    lateinit var repository: Repository

    init {
        App.component.inject(this)
    }

    private val _index = MutableLiveData<List<WorkoutDay>>().apply {

    }
    val text: LiveData<List<WorkoutDay>> = _index

    private val _weekWorkout = MutableLiveData<List<WorkoutDay>>().apply {
//        if (program != null) {
//            repository.getWorkoutDayById(program.id_workoutDay)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({ list ->
//                    value = list
//
//                }, { e ->
//
//                })
//        }

    }
    val weekWorkout: LiveData<List<WorkoutDay>> = _weekWorkout

    fun setIndex(index: Int,workoutDay: List<WorkoutDay> ) {
        when (index){
            1 -> _index.value = workoutDay
            2 -> _index.value = workoutDay
            3 -> _index.value = workoutDay
            4 -> _index.value = workoutDay
        }
    }

}