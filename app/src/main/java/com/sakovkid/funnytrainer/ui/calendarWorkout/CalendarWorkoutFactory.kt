package com.sakovkid.funnytrainer.ui.calendarWorkout

import androidx.annotation.NonNull
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sakovkid.funnytrainer.data.repository.model.Program

class CalendarWorkoutFactory(val program: Program) :
    ViewModelProvider.NewInstanceFactory() {
    @NonNull
    override fun <T : ViewModel?> create(@NonNull modelClass: Class<T>): T {
        if (modelClass == CalendarWorkoutViewModel::class.java) {
            return CalendarWorkoutViewModel(program) as T
        } else {
            return CalendarWorkoutViewModel(null) as T
        }
    }
}