package com.sakovkid.funnytrainer.ui.activity.splashScreen

import android.view.animation.Animation

class onAnimationListener(var onAnimationEnd:() -> Unit): Animation.AnimationListener {
    override fun onAnimationStart(animation: Animation?) {}

    override fun onAnimationEnd(animation: Animation?) {
        onAnimationEnd()
    }

    override fun onAnimationRepeat(animation: Animation?) {}
}