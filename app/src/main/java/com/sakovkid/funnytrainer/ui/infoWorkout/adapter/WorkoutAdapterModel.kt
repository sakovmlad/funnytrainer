package com.sakovkid.funnytrainer.ui.infoWorkout.adapter

data class WorkoutAdapterModel(
    val id: Long,
    val name: String,
    val count_time: Int,
    val count_reps: Int,
    val description: String,
    val url_gif: String,
    val url_video: String
)