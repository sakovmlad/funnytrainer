package com.sakovkid.funnytrainer.ui.infoWorkout.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.sakovkid.funnytrainer.R
import pl.droidsonroids.gif.GifImageView

class WorkoutAdapter(private val listener: OnWorkoutClickListener) :
    RecyclerView.Adapter<WorkoutAdapter.ViewHolder>() {

    private var POSITION = 0

    private var workout: MutableList<WorkoutAdapterModel> = mutableListOf()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var nameWorkout: TextView = itemView.findViewById(R.id.tv_name_workout)
        private var timeWorkout: TextView = itemView.findViewById(R.id.tv_time_workout)
        private var imageProgram:GifImageView = itemView.findViewById(R.id.gif_workout)
        private var viewWorkout:ConstraintLayout = itemView.findViewById(R.id.view_workout)


        fun bind(workoutAdapterModel: WorkoutAdapterModel, listener: OnWorkoutClickListener) {
            nameWorkout.text = workoutAdapterModel.name
            viewWorkout.setOnClickListener {
                listener.onClickWorkout(workoutAdapterModel)
            }


//            val d = 0.3
//            nameProgram.text = workout.name
//            lastTime.text = "Последний раз: Сегодня"
//            Picasso.with(itemView.context)
//                .load(workout.src)
//                .centerCrop()
//                .resize((program.width * d).toInt(), (photo.height * d).toInt())
//                .into(imageProgram)
//            cardView.setOnClickListener {
//                listener.onClickWorkout(workout)
//            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_workout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(workout[position], listener)
    }

    override fun getItemCount() = workout.size

    fun addDataSet(workout: List<WorkoutAdapterModel>) {
        this.workout.addAll(workout)
        notifyItemRangeChanged(POSITION, workout.count())
        POSITION += workout.count()
    }

    fun replaceDataSet(workout: List<WorkoutAdapterModel>) {
        this.workout.clear()
        this.workout.addAll(workout)
        POSITION = workout.count() - 1
        notifyDataSetChanged()
    }

    fun getList(): List<WorkoutAdapterModel> {
        return workout

    }


}