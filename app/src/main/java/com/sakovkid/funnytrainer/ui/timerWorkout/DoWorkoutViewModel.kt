package com.sakovkid.funnytrainer.ui.timerWorkout

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sakovkid.funnytrainer.app.App
import com.sakovkid.funnytrainer.service.MediaPlayer
import javax.inject.Inject

class DoWorkoutViewModel : ViewModel() {

    @Inject
    lateinit var mediaPlayer: MediaPlayer

    init {
        App.component.inject(this)
    }

    private val _time = MutableLiveData<String>().apply {

    }
    val time: LiveData<String> = _time

}