package com.sakovkid.funnytrainer.ui.timerBegin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sakovkid.funnytrainer.app.App
import com.sakovkid.funnytrainer.data.repository.model.WorkoutDay
import com.sakovkid.funnytrainer.service.Timer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class BeginWorkoutViewModel : ViewModel() {
    @Inject
    lateinit var timer: Timer

    init {
        App.component.inject(this)
    }

    private val _progress = MutableLiveData<Int>().apply {
        timer.create(15000, 1000, {
            value = it
        }, { value = it })
        timer.start()
    }
    val progress: LiveData<Int> = _progress
}