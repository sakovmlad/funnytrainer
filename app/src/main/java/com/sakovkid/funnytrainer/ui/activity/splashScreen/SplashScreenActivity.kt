package com.sakovkid.funnytrainer.ui.activity.splashScreen

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.sakovkid.funnytrainer.R
import com.sakovkid.funnytrainer.app.App
import com.sakovkid.funnytrainer.data.database.entity.WorkoutType
import com.sakovkid.funnytrainer.data.repository.*
import com.sakovkid.funnytrainer.data.repository.model.*
import com.sakovkid.funnytrainer.ui.activity.main.MainActivity
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SplashScreenActivity : AppCompatActivity() {

    @Inject
    lateinit var repository: Repository

    private var FIRST_LAUNCH = "FIRST_LAUNCH"

    init {
        App.component.inject(this)
    }

    private lateinit var topAnim: Animation
    private lateinit var image: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_spalsh_screen)
        initField()
    }
    private fun initField(){
        topAnim = AnimationUtils.loadAnimation(this, R.anim.top_animation)
        image = findViewById(R.id.image_splash)
        image.startAnimation(topAnim)
        topAnim.setAnimationListener(onAnimationListener { isAppFirstRun() })
    }


    private fun isAppFirstRun() {
        val intent = Intent(this, MainActivity::class.java)
        val sPref: SharedPreferences = getPreferences(MODE_PRIVATE)
        if (sPref.getBoolean(FIRST_LAUNCH, true)) {
            val editor = sPref.edit()
            editor.putBoolean(FIRST_LAUNCH, false)
            editor.apply()
            fillDatabase(intent)
        } else {
            Thread.sleep(2000)
            startActivity(intent)
            finish()
        }
    }

    private fun fillDatabase(intent: Intent) {
        val program: Program = fillProgramModel()
        val workoutDay: List<WorkoutDay> = fillWorkoutDayModel()
        val workout: List<Workout> = fillWorkoutModel()
        val warmUp: List<WarmUp> = fillWarmUpModel()
        val hitch: List<Hitch> = fillHitchModel()
        val user = User()

        repository.setUserToDb(user)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ },{})

        Single.concat(
            repository.setWorkoutToDb(workout),
            repository.setHitchToDb(hitch),
            repository.setWarmUpToDb(warmUp),
            repository.setWorkoutDayToDb(workoutDay)
        ).subscribeOn(Schedulers.io())
            .subscribe({},
            {
                Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
            }, {
                repository
                    .setProgramToDb(program)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        startActivity(intent)
                        finish()
                               }, { error ->
                        Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
                    })
            })
    }

    private fun fillHitchModel(): List<Hitch> {
        val list: MutableList<Hitch> = mutableListOf()
        list.add(
            Hitch(
                1,
                "Растяжка змея",
                60,
                "Лежим и шипим :D",
                "https:/2",
                "https:/77"
            )
        )
        list.add(
            Hitch(
                2,
                "Боковая растяжка",
                30,
                "Лежим и спим",
                "https:/2",
                "https:/77"
            )
        )
        return list
    }

    private fun fillWarmUpModel(): List<WarmUp> {
        val list: MutableList<WarmUp> = mutableListOf()
        list.add(
            WarmUp(
                1,
                "Скалолаз",
                30,
                "Руки на пол, ногами бежать",
                "https://sadsss",
                "https://sdw"
            )
        )
        list.add(
            WarmUp(
                2,
                "Махи руками",
                14,
                "Мажем руками как дебил и орем",
                "https://sadsss",
                "https://sdw"
            )
        )
        return list
    }

    private fun fillWorkoutModel(): List<Workout> {
        val list: MutableList<Workout> = mutableListOf()
        list.add(
            Workout(
                1,
                "Отжимания",
                WorkoutType.REPS,
                20,
                0,
                "Здесь описание",
                "http://sdasd",
                "https://sad"
            )
        )
        list.add(
            Workout(
                2,
                "Планка",
                WorkoutType.TIME,
                0,
                60,
                "Здесь описание",
                "http://sdasd",
                "https://sad"
            )
        )
        return list
    }

    private fun fillWorkoutDayModel(): List<WorkoutDay> {
        val a: MutableList<WorkoutDay> = mutableListOf()
        a.add(WorkoutDay(1,"День 1",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(2,"День 2",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(3,"День 3",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(4,"День 4",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(5,"День 5",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(6,"День 6",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(7,"День 7",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(8,"День 8",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(9,"День 9",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(10,"День 10",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(11,"День 11",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(12,"День 12",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(13,"День 13",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(14,"День 14",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(15,"День 15",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(16,"День 16",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(17,"День 17",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(18,"День 18",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(19,"День 19",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(20,"День 20",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(21,"День 21",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(22,"День 22",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(23,"День 23",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(24,"День 24",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(25,"День 25",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(26,"День 26",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(27,"День 27",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
        a.add(WorkoutDay(28,"День 28",false,System.currentTimeMillis(),1,1,60,listOf(1, 2),listOf(1, 2),listOf(1, 2)))
return a
    }

    private fun fillProgramModel(): Program {
        return Program(
            1,
            "Похудеть к лету",
            "http",
            28,
            listOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28)
        )
    }

}