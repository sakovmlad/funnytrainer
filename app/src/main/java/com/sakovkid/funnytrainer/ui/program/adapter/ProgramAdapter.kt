package com.sakovkid.funnytrainer.ui.program.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.storage.FirebaseStorage
import com.sakovkid.funnytrainer.R
import com.sakovkid.funnytrainer.data.repository.model.Program
import com.squareup.picasso.Picasso

class ProgramAdapter(private val listener: OnProgramClickListener) :
    RecyclerView.Adapter<ProgramAdapter.ViewHolder>() {

    private var POSITION = 0

    private var program: MutableList<Program> = mutableListOf()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var nameProgram: TextView = itemView.findViewById(R.id.text_name_program)
        private var lastTime: TextView = itemView.findViewById(R.id.text_last_time)
        private var imageProgram:ImageView = itemView.findViewById(R.id.image_program)
        private var cardView:ConstraintLayout = itemView.findViewById(R.id.view_program)

        fun bind(program: Program, listener: OnProgramClickListener) {
//            val d = 0.3
            nameProgram.text = program.name
            lastTime.text = "Последний раз: Сегодня"
            val a = FirebaseStorage.getInstance().reference.root.child(program.url_image).downloadUrl.addOnSuccessListener {
                Picasso.with(itemView.context)
                    .load("https://img3.goodfon.ru/original/1024x600/b/a2/background-cvet-raduga-spektr.jpg")
                    .into(imageProgram)
            }
            cardView.setOnClickListener {
                listener.onClickProgram(program)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_program, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(program[position], listener)
    }

    override fun getItemCount() = program.size

    fun addDataSet(program: List<Program>) {
        this.program.addAll(program)
        notifyItemRangeChanged(POSITION, program.count())
        POSITION += program.count()
    }

    fun replaceDataSet(program: List<Program>) {
        this.program.clear()
        this.program.addAll(program)
        POSITION = program.count() - 1
        notifyDataSetChanged()
    }


}