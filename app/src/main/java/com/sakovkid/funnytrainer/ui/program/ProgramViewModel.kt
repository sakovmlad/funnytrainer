package com.sakovkid.funnytrainer.ui.program

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sakovkid.funnytrainer.app.App
import com.sakovkid.funnytrainer.data.repository.model.Program
import com.sakovkid.funnytrainer.data.repository.Repository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProgramViewModel(): ViewModel() {


    @Inject
    lateinit var repository: Repository

    init {
        App.component.inject(this)
    }

    private val _text = MutableLiveData<String>().apply {
        repository.getProgram()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                       val a = it
            },{
                val b = it
            })
    }
    val text: LiveData<String> = _text

    private val _programResult = MutableLiveData<List<Program>>().apply {
        repository
            .getProgram()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ it ->
                value = it                //hideProgress
            }, { e ->
                //showError(e)
            })
    }
    val program: LiveData<List<Program>> = _programResult
}