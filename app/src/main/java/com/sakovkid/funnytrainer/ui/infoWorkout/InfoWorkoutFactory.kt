package com.sakovkid.funnytrainer.ui.infoWorkout

import androidx.annotation.NonNull
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sakovkid.funnytrainer.data.repository.model.WorkoutDay

class InfoWorkoutFactory(val workoutDay: WorkoutDay) :
    ViewModelProvider.NewInstanceFactory() {
    @NonNull
    override fun <T : ViewModel?> create(@NonNull modelClass: Class<T>): T {
        if (modelClass == InfoWorkoutViewModel::class.java) {
            return InfoWorkoutViewModel(workoutDay) as T
        } else {
            return InfoWorkoutViewModel(null) as T
        }
    }
}