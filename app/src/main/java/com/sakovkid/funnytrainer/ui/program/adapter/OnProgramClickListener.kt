package com.sakovkid.funnytrainer.ui.program.adapter

import com.sakovkid.funnytrainer.data.repository.model.Program

interface OnProgramClickListener {
    fun onClickProgram(program: Program)
}