package com.sakovkid.funnytrainer.ui.calendarWorkout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.sakovkid.funnytrainer.R
import com.sakovkid.funnytrainer.data.repository.model.Program
import com.sakovkid.funnytrainer.data.repository.model.WorkoutDay
import com.sakovkid.funnytrainer.ui.calendarWorkout.adapter.SectionsPagerAdapter

class CalendarWorkoutFragment : Fragment() {


    private lateinit var calendarWorkoutViewModel: CalendarWorkoutViewModel

    private lateinit var workoutDay: WorkoutDay
    private lateinit var program: Program


    companion object {

        private const val ARG_SECTION_NUMBER = "section_number"
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_calendar_workout, container, false)
        program = this.arguments?.getSerializable("program") as Program
//



        calendarWorkoutViewModel = ViewModelProvider(this, CalendarWorkoutFactory(program))
            .get(CalendarWorkoutViewModel::class.java).apply {
//                setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
            }


        calendarWorkoutViewModel.workout.observe(viewLifecycleOwner, Observer { list ->

            val sectionsPagerAdapter = SectionsPagerAdapter(this.requireContext(), childFragmentManager,list)
            val viewPager: ViewPager = root.findViewById(R.id.view_pager)
            viewPager.adapter = sectionsPagerAdapter
            val tabs: TabLayout = root.findViewById(R.id.tabs)
            tabs.setupWithViewPager(viewPager)

        })

//        calendarWorkoutViewModel.workoutDay.observe(viewLifecycleOwner, Observer {
//            calendar.selectedDates = it
//        })

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}