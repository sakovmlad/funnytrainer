package com.sakovkid.funnytrainer.ui.infoWorkout.adapter

interface OnWorkoutClickListener {
    fun onClickWorkout(workoutAdapterModel: WorkoutAdapterModel)
}