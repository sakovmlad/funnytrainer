package com.sakovkid.funnytrainer.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.sakovkid.funnytrainer.app.App
import com.sakovkid.funnytrainer.data.repository.Repository
import com.sakovkid.funnytrainer.data.repository.model.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProfileViewModel : ViewModel() {
@Inject
lateinit var repository: Repository

init {
    App.component.inject(this)
}
    private val _user = MutableLiveData<User>().apply {
        repository.getUserToDb(FirebaseAuth.getInstance().uid ?: "")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({value = it},{})

    }
    val user: LiveData<User> = _user


}