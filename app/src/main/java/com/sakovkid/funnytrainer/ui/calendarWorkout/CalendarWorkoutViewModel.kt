package com.sakovkid.funnytrainer.ui.calendarWorkout

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sakovkid.funnytrainer.app.App
import com.sakovkid.funnytrainer.data.repository.model.Program
import com.sakovkid.funnytrainer.data.repository.Repository
import com.sakovkid.funnytrainer.data.repository.model.WorkoutDay
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CalendarWorkoutViewModel(val program: Program?) : ViewModel() {

    @Inject
    lateinit var repository: Repository

    init {
        App.component.inject(this)
    }

    private val _workoutResult = MutableLiveData<List<WorkoutDay>>().apply {
        if (program != null) {
            repository.getWorkoutDayById(program.id_workoutDay)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ list ->
                    value = list

                }, { e ->

                })
        }

    }
    val workout: LiveData<List<WorkoutDay>> = _workoutResult



}