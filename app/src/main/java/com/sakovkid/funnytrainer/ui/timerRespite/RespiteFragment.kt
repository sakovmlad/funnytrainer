package com.sakovkid.funnytrainer.ui.timerRespite

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sakovkid.funnytrainer.R

class RespiteFragment : Fragment() {

    companion object {
        fun newInstance() = RespiteFragment()
    }

    private lateinit var viewModel: RespiteViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.respite_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(RespiteViewModel::class.java)
        // TODO: Use the ViewModel
    }


    private fun convertToString(time: Long): String {

        var textTime = "00:00"

        textTime = if ((time / 60).toString().length == 1) "0" + (time / 60).toString()
        else (time / 60).toString()

        textTime = if ((time % 60).toString().length == 1) textTime + ":0" + (time % 60).toString()
        else textTime + ":" + (time % 60).toString()

        return textTime
    }
}