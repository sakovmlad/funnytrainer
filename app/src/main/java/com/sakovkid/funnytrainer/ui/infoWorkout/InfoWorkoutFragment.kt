package com.sakovkid.funnytrainer.ui.infoWorkout

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.sakovkid.funnytrainer.R
import com.sakovkid.funnytrainer.data.repository.model.WorkoutDay
import com.sakovkid.funnytrainer.ui.dialogScreen.DialogScreen
import com.sakovkid.funnytrainer.ui.infoWorkout.adapter.OnWorkoutClickListener
import com.sakovkid.funnytrainer.ui.infoWorkout.adapter.WorkoutAdapter
import com.sakovkid.funnytrainer.ui.infoWorkout.adapter.WorkoutAdapterModel

class InfoWorkoutFragment : Fragment() {

    lateinit var countWorkout: TextView
    lateinit var timeWorkout: TextView
    lateinit var workoutDay: WorkoutDay
    lateinit var btnGoTrening: Button

    companion object {
        fun newInstance() = InfoWorkoutFragment()
    }

    private lateinit var viewModel: InfoWorkoutViewModel

    private var adapter: WorkoutAdapter = WorkoutAdapter(object : OnWorkoutClickListener {
        override fun onClickWorkout(workoutAdapterModel: WorkoutAdapterModel) {
            val dialog = DialogScreen()
            dialog.buildDescription(this@InfoWorkoutFragment.requireContext(), workoutAdapterModel)
        }
    })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_info_workout, container, false)
        val workoutDay = this.arguments?.getSerializable("workout") as WorkoutDay

        countWorkout = root.findViewById(R.id.tv_count_workout)
        timeWorkout = root.findViewById(R.id.tv_full_time)

        btnGoTrening = root.findViewById(R.id.btn_begin_workout)

        btnGoTrening.setOnClickListener {
            findNavController().navigate(R.id.beginWorkoutFragment)
        }

        viewModel = ViewModelProvider(
            this,
            InfoWorkoutFactory(workoutDay)
        ).get(InfoWorkoutViewModel::class.java)
        val count =
            workoutDay.id_hitch.count() + workoutDay.id_warmUp.count() + workoutDay.id_workout.count()
        countWorkout.text = count.toString()
        timeWorkout.text = workoutDay.time_workout.toString()

        viewModel.workout.observe(viewLifecycleOwner, Observer {
            adapter.addDataSet(it)
        })
        viewModel.time.observe(viewLifecycleOwner, Observer {
            timeWorkout.text = it
        })

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewWorkout = view.findViewById<RecyclerView>(R.id.recycler_workout)
        viewWorkout.adapter = adapter

    }

}