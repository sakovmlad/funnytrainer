package com.sakovkid.funnytrainer.ui.dialogScreen

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.Window
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import com.sakovkid.funnytrainer.R
import com.sakovkid.funnytrainer.ui.infoWorkout.adapter.WorkoutAdapterModel
import pl.droidsonroids.gif.GifImageView

class DialogScreen {
    fun buildDescription(context: Context,workout: WorkoutAdapterModel){
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.diaolog_descriprion)
        val textDesc = dialog.findViewById<TextView>(R.id.tv_descriprion)
        val gifDesc = dialog.findViewById<GifImageView>(R.id.gif_description)
        val textNameDesc = dialog.findViewById<TextView>(R.id.tv_name_description)
        textDesc.text = workout.description
        textNameDesc.text = workout.name
//        gifDesc = workout.url_gif
        dialog.show()
    }
    fun buildDialogExit(activity: Activity, navController: NavController){
        val dialog = Dialog(activity,android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_timer_exit)
        val constLayoutHard = dialog.findViewById<ConstraintLayout>(R.id.view_hard)
        val constLayoutYes = dialog.findViewById<ConstraintLayout>(R.id.view_yes)
        val constLayoutCancel = dialog.findViewById<ConstraintLayout>(R.id.view_cancel)
        constLayoutHard.setOnClickListener { }
        constLayoutYes.setOnClickListener {
            dialog.onBackPressed()
            navController.popBackStack()
        }
        constLayoutCancel.setOnClickListener { dialog.onBackPressed() }
        dialog.show()
    }
}