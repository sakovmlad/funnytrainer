package com.sakovkid.funnytrainer.ui.timerBegin

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import com.sakovkid.funnytrainer.R
import kotlin.concurrent.timer

class BeginWorkoutFragment : Fragment() {

    companion object {
        fun newInstance() = BeginWorkoutFragment()
    }

    private lateinit var viewModel: BeginWorkoutViewModel
    private lateinit var progressBar: ProgressBar
    private lateinit var btnCancelTimer: ImageButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.begin_workout_fragment, container, false)
        progressBar = root.findViewById(R.id.progress_bar_circle)
        btnCancelTimer = root.findViewById(R.id.btn_timer_off)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(BeginWorkoutViewModel::class.java)
        viewModel.progress.observe(viewLifecycleOwner, Observer {
            progressBar.progress = it
        })
        btnCancelTimer.setOnClickListener {
            viewModel.timer.destroy()
        }
    }

    override fun onDestroy() {
        viewModel.timer.destroy()
        super.onDestroy()
    }


}