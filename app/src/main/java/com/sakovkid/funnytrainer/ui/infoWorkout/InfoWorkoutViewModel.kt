package com.sakovkid.funnytrainer.ui.infoWorkout

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sakovkid.funnytrainer.app.App
import com.sakovkid.funnytrainer.data.repository.Repository
import com.sakovkid.funnytrainer.data.repository.model.WorkoutDay
import com.sakovkid.funnytrainer.ui.infoWorkout.adapter.WorkoutAdapterModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class InfoWorkoutViewModel(val workoutDay: WorkoutDay?) : ViewModel() {
    var timeWorkout:Long = 0

    @Inject
    lateinit var repository: Repository

    init {
        App.component.inject(this)
    }

    private val _workoutResult = MutableLiveData<List<WorkoutAdapterModel>>().apply {
        if (workoutDay != null) {
            repository.getWorkoutAdapterModule(
                workoutDay.id_warmUp,
                workoutDay.id_workout,
                workoutDay.id_hitch
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    value = it
                    it.forEach {
                        timeWorkout += it.count_reps * 2 + it.count_time
                        _time.value = timeToString(timeWorkout)
                    }
                }, {

                })
        }
    }
    val workout: LiveData<List<WorkoutAdapterModel>> = _workoutResult

    private val _time = MutableLiveData<String>().apply {

    }
    val time: LiveData<String> = _time

    private fun timeToString(sec: Long): String {
        val hour = sec / 3600
        val min = sec / 60 % 60
        val sec = sec / 1 % 60
        return String.format("%02d:%02d:%02d", hour, min, sec)
    }
}