package com.sakovkid.funnytrainer.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputEditText
import com.sakovkid.funnytrainer.R

class ProfileFragment : Fragment() {

    lateinit var textId: TextView
    lateinit var textName: EditText
    lateinit var textAllKalories: EditText
    lateinit var textProgram: EditText
    lateinit var textTime: EditText
    lateinit var textWorkout: EditText

    private lateinit var profileViewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_profile, container, false)
        textId = root.findViewById(R.id.text_input_id)
        textName = root.findViewById(R.id.text_input_name)
        textAllKalories = root.findViewById(R.id.text_input_kalories)
        textProgram = root.findViewById(R.id.text_input_program)
        textTime = root.findViewById(R.id.text_input_time)
        textWorkout = root.findViewById(R.id.text_input_workout)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        profileViewModel =
            ViewModelProvider(this).get(ProfileViewModel::class.java)

        profileViewModel.user.observe(viewLifecycleOwner, Observer {
            textId.setText(it.id)
            textName.setText(it.name)
            textAllKalories.setText(it.count_k_calories.toString())
            textWorkout.setText(it.count_workout_completed.toString())
        })
    }
}