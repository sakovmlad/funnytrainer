package com.sakovkid.funnytrainer.service

import android.os.CountDownTimer

class Timer(
) {
    private var timer: CountDownTimer? = null
    private var currentTime: Long = 0
    private var beginTime: Long = 0
    private var tickTime: Long = 0
    private var requestTick: (Int) -> Unit = {}
    private var requestFinish: (Int) -> Unit = {}

    fun create(
        beginTime: Long,
        tickTime: Long,
        requestTick: (Int) -> Unit,
        requestFinish: (Int) -> Unit
    ) {
        this.beginTime = beginTime
        this.tickTime = tickTime
        this.requestTick = requestTick
        this.requestFinish = requestFinish

        if (timer == null) {
            currentTime = beginTime
            timer = object : CountDownTimer(beginTime, tickTime) {
                override fun onTick(millisUntilFinished: Long) {
                    currentTime -= tickTime
                    requestTick((currentTime / 1000).toInt())
                }

                override fun onFinish() {
                    requestFinish(0)
                    destroy()
                }
            }
        }
    }

    fun start() {
        timer?.start()
    }

    fun destroy() {
        pause()
        timer = null
        requestFinish(0)
    }

    fun pause() {
        timer?.cancel()
    }

    fun plusTime(plusTime: Long) {
        timer?.cancel()
        currentTime += plusTime
        replaceTimer(currentTime)
    }

    private fun replaceTimer(time:Long) {
        currentTime = time
        timer = object : CountDownTimer(time, tickTime) {
            override fun onTick(millisUntilFinished: Long) {
                currentTime -= tickTime
                requestTick((currentTime / 1000).toInt())
            }

            override fun onFinish() {
                requestFinish(0)
                destroy()
            }
        }
    }


}