package com.sakovkid.funnytrainer.service

import android.content.Context
import android.media.MediaPlayer
import com.sakovkid.funnytrainer.R

class MediaPlayer(val context: Context) {

    fun playSoundFinish() {
        MediaPlayer.create(context, R.raw.shot).start()
    }

    fun playSoundNumber(text: String) {
        when (text) {
            "00:05" -> MediaPlayer.create(context, R.raw.shot).start()
            "00:04" -> MediaPlayer.create(context, R.raw.shot).start()
            "00:03" -> MediaPlayer.create(context, R.raw.shot).start()
            "00:02" -> MediaPlayer.create(context, R.raw.shot).start()
            "00:01" -> MediaPlayer.create(context, R.raw.shot).start()
        }
    }
}